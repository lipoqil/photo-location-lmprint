#!/usr/bin/env ruby

require 'mini_exiftool'
require 'rmagick'
require './config'

require 'ruby-progressbar'
using ProgressBar::Refinements::Enumerator


sharp_text = Magick::Draw.new
sharp_text.font_family = $config[:font_name]
sharp_text.gravity = Magick::SouthEastGravity
sharp_text.fill = 'white'
sharp_text.undercolor = Magick::Pixel.new(0, 0, 0, Magick::QuantumRange/$config[:font_background_opacity_divisor])

original_photos = Dir.glob("#{$config[:input_directory] || './photos'}/*.{jpg,jpeg}")
original_photos.each.with_progressbar do |photo_filename, photos_progressbar|
  photo_basename = File.basename(photo_filename)
  photos_progressbar.title = photo_basename[0..19]&.ljust(20, ' ')

  # Extract metadata
  photo = MiniExiftool.new photo_filename, coord_format: '%.6f'
  photo_location = [photo.location, photo.city, nil, photo.country].compact.join(', ')

  magic_photo = Magick::ImageList.new photo_filename
  sharp_text.annotate(magic_photo, 0,0,0,0, photo_location) do |options|
    options.pointsize = magic_photo.rows/$config[:font_size_divisor]
  end

  new_file_path = File.join(($config[:output_directory] || './tmp'), photo_basename)
  magic_photo.write new_file_path
end
