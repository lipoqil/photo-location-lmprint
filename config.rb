require 'optionparser'

$config = YAML.load_file 'config.yaml', symbolize_names: true

OptionParser.new do |opts|
  opts.banner = "Usage: #{$1} [options]"

  opts.on("-i", "--input-directory=INPUT-DIRECTORY", "Where to take images from") do |v|
    $config[:input_directory] = v
  end
  opts.on("-o", "--output-directory=OUTPUT-DIRECTORY", "Where to put images after editing") do |v|
    $config[:output_directory] = v
  end
end.parse!
