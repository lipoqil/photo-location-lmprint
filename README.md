# Photo location imprint

Imprints location in [EXIF](https://en.wikipedia.org/wiki/Exif) into visible part of the image

## Usage

### Photos in this project

- place your tagged images into photos directory
- run `./imprint.rb` in directory of this project
- your new photos should be in _./tmp_

### Photos somewhere else

Provide CLI arguments:
- `--input-directory` &mdash; Photos will be taken from here
- `--output-directory` &mdash; Photos will saved here after processing

#### Example

`./imprint.rb --input-directory /private/tmp/Wall\ copy --output-directory /private/tmp/Wall\ out`

## Setup

- the builder is written in Ruby, so you'll need [Ruby](https://www.ruby-lang.org) installed. The builder was written and tested on Ruby 3.3.0
- system dependencies are described in _Brewfile_, so if you have [Homebrew](https://brew.sh) installed, you can install the system dependencies by running `brew bundle`
- this tool is using some Ruby gems, which can be installed via [Bundler](https://bundler.io) by running `bundle`
- copy _[config.yaml.example](config.yaml.example)_ into _[config.yaml](config.yaml)_

### Configuration

Used configuration resides in config.yaml file.

- _font_size_divisor_ &mdash; Font size is calculated from photo's height which is divided by value in this 
  configuration key
- _font_background_opacity_divisor_ &mdash; Dark text's background has opacity of maximal possible opacity, divided by 
  value in this configuration key 
- _font_name_ &mdash; Font name ¯\_(ツ)_/¯

## Example output

![frk](https://i.imgur.com/6vftznH.jpg)
